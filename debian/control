Source: dbus-cpp
Priority: optional
Maintainer: Debian UBports Team <team+ubports@tracker.debian.org>
Uploaders:
 Mike Gabriel <sunweaver@debian.org>,
Build-Depends: cmake,
               cmake-extras,
               dbus,
               debhelper-compat (= 13),
               doxygen,
               google-mock,
               libboost-filesystem-dev,
               libboost-system-dev,
               libboost-program-options-dev,
               libdbus-1-dev,
               libgtest-dev,
               libprocess-cpp-dev,
               libproperties-cpp-dev,
               libxml2-dev,
               lsb-release,
               pkg-kde-tools,
Standards-Version: 4.5.1
Rules-Requires-Root: no
Section: libs
Homepage: https://gitlab.com/ubports/code/lib-cpp/dbus-cpp
Vcs-Git: https://salsa.debian.org/ubports-team/dbus-cpp.git
Vcs-Browser: https://salsa.debian.org/ubports-team/dbus-cpp

Package: libdbus-cpp5
Architecture: linux-any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: header-only dbus-binding leveraging C++-11 (runtime libraries)
 A header-only dbus-binding leveraging C++-11, relying on compile-time
 polymorphism to integrate with arbitrary type systems. Runtime portions to
 bind to different event loops.

Package: dbus-cpp-bin
Section: libdevel
Architecture: linux-any
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libdbus-cpp5 (= ${binary:Version})
Description: header-only dbus-binding leveraging C++-11 (tools)
 Protocol compiler and generator to automatically generate protocol headers
 from introspection XML.
 .
 D-Bus is a message bus used for sending messages between applications.
 .
 This package contains runtime binaries.

Package: libdbus-cpp-dev
Section: libdevel
Architecture: linux-any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         dbus,
         libboost-filesystem-dev,
         libboost-system-dev,
         libboost-program-options-dev,
         libdbus-1-dev,
         libdbus-cpp5 (= ${binary:Version}),
         libproperties-cpp-dev,
Replaces: dbus-cpp-dev
Conflicts: dbus-cpp-dev
Provides: dbus-cpp-dev
Description: header-only dbus-binding leveraging C++-11 (development files)
 A header-only dbus-binding leveraging C++-11, relying on compile-time
 polymorphism to integrate with arbitrary type systems.
 .
 D-Bus is a message bus used for sending messages between applications.

Package: dbus-cpp-dev-examples
Section: x11
Architecture: linux-any
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: header-only dbus-binding leveraging C++-11 (examples)
 A header-only dbus-binding leveraging C++-11, relying on compile-time
 polymorphism to integrate with arbitrary type systems.
 .
 D-Bus is a message bus used for sending messages between applications.
 .
 This package provides examples.
